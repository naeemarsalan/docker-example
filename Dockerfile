FROM gcr.io/kaniko-project/executor:v1.8.0
FROM alpine
COPY --from=0 /kaniko/executor /usr/local/bin/executor
